package com.example.compu2.calculadora;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    private EditText Num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Num = (EditText)findViewById(R.id.Num);
    }

    public void btnEliminar (View view){
        Num.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void SetTextOnEditText(View view){
        Button btnPressed = (Button)view;
        Num.setText(Num.getText().toString() + btnPressed.getText().toString());
    }


    public void btnIgualOperar(View view){
        try {
            String operacion = Num.getText().toString();
            double intOperacion = Operaciones.operar(Num.getText().toString());
            Num.setText(String.valueOf(intOperacion));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
